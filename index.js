const admin = require('firebase-admin');
admin.initializeApp();

const schedule = require('./schedule');
const model = require('./model');
const misc = require('./miscellaneus');

module.exports = {
    doubles: schedule.doubles,
    removeSub: schedule.removeSub,
    modelCleanup: schedule.modelCleanup,
    summary: schedule.summary,
    model: model.poorModel,
    richModel: model.richModel,
    orgIndex: model.orgIndex,
    insertMember: misc.insertMember,
    isNameFree: misc.isNameFree,
    premium: misc.premium
}
