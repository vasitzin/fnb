const env = require('./env.json');
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const stripe = require('stripe')(env.stripeKey);
const email = require('nodemailer');
const common = require('./model/common');
const model = require('./model/common').model;

const admins = env.admins;

let transp = email.createTransport({
  host: env.email.host,
  port: 587,
  secure: false,
  auth: {
    user: env.email.user,
    pass: env.email.pass
  },
  tls: {
    rejectUnauthorized: false
  }
});

let message = {
  from: env.email.user,
  to: admins,
  subject: "",
  text: ""
};

const removeSub = functions.pubsub.schedule('0 0 1 * *')
  .timeZone('Europe/Athens')
  .onRun(async () => {
    await admin.firestore().collection('premiumIndex').get()
      .then(async (querySnap) => {
        const docs = querySnap.docs;
        let sids = [];
        for (doc of querySnap.docs) sids.push(doc.data().subscription);
        for (sid of sids) {
          if (sid !== env.freepass) {
            var sub = await stripe.subscriptions.retrieve(sid);
            if (sub.status != 'active')
              await admin.firestore().collection('premiumIndex')
                .doc(docs[sids.indexOf(sid)].id)
                .delete();
          }
        }
      }).catch(error => { console.error(error); });
    return null;
  });

const doubles = functions.pubsub.schedule('0 12 * * *')
  .timeZone('Europe/Athens')
  .onRun(async () => {
    let send = false;
    const querySnap = await admin.firestore().collection('premiumIndex').get().catch(/*TODO*/);
    let uids = []; // uid array.
    let sids = []; // subscription id array.

    message.subject = "Sent from doubles pubsub.";
    message.text = "On premiumIndex checkup for doubles.\n";

    // fill above arrays.
    for (doc of querySnap.docs) {
      uids.push(doc.data().uid);
      sids.push(doc.data().subscription);
    }

    let matches = new Map();
    for (uid of uids) {
      var hits = 0;
      var pos = uids.indexOf(uid); // first instance of the uid
      while (pos != -1) {
        hits++;
        pos = uids.indexOf(uid, pos + 1);
      }
      if (hits > 1 && !matches.has(uid)) {
        matches.set(uid, hits);
        console.error('[PS01] Found ' + hits + " instances of uid: " + uid);
        message.text += "Found " + hits + " instances of uid: " + uid + "\n";
        send = true;
      }
    }

    matches = new Map();
    for (sid of sids) {
      var hits = 0;
      var pos = sids.indexOf(sid); // first instance of the sub-id
      while (pos != -1) {
        hits++;
        pos = sids.indexOf(sid, pos + 1);
      }
      if (hits > 1 && !matches.has(sid)) {
        matches.set(sid, hits);
        console.error('[PS01] Found ' + hits + " instances of subscription: " + sid);
        message.text += "Found " + hits + " instances of subscription id: " + sid + "\n";
        send = true;
      }
    }

    if (send)
      transp.sendMail(message, (error, data) => { if (error) console.error('Email failed to send.'); });

    return null;
  });

const modelCleanup = functions.pubsub.schedule('5 12 * * *')
  .timeZone('Europe/Athens')
  .onRun(async () => {
    let deleteDoc = common.deleteDoc;
    let level = 2; // the 'level' in the model we want to check.

    while (level < model.length) {
      await admin.firestore().collection(model[level]).get()
        .then(async (querySnap) => {

          for (var doc of querySnap.docs) {
            let pid = doc.data().pid;

            switch (model[level]) {

              case model[2]:
                await admin.auth().getUser(pid)
                  .catch(async (error) => {
                    if (error.code === 'auth/user-not-found')
                      await deleteDoc(model[2], doc.id, true)
                        .then(() => {
                          console.warn(error.code + ' Document deleted in collection: ' + model[level]);
                        }).catch(error => { console.error(error); });
                    else console.error('[5G3P] Couldn\'t get user.\n' +
                      error.code + '\n'
                      + error);
                  });
                break;

              case model[3]:
                await admin.firestore().collection(model[level - 1])
                  .doc(pid).get().then(async (ref) => {
                    if (!ref.exists) await deleteDoc(model[level], doc.id, true)
                      .then(() => {
                        console.warn('Document deleted in collection: ' + model[level]);
                      });
                  }).catch(error => { console.error(error); });

              case model[4]:
                await admin.firestore().collection(model[level - 1])
                  .doc(pid).get().then(async (ref) => {
                    if (!ref.exists) await deleteDoc(model[level], doc.id, false)
                      .then(() => {
                        console.warn('Document deleted in collection: ' + model[level]);
                      });
                  }).catch(error => { console.error(error); });

              default:
                break;
            }
          }
        });
      level++;
    }
    return null;
  });

const summary = functions.pubsub.schedule('0 0 1 * *')
  .timeZone('Europe/Athens')
  .onRun(async () => {
    const curYear = new Date().getUTCFullYear();
    const prevMonth = new Date().getUTCMonth() - 1;
    const targetDate = new Date(Date.UTC(curYear, prevMonth, 1));
    const targetMilli = targetDate.valueOf();
    const orgQuerySnap = await admin.firestore().collection(model[0]).get() // get all organizations (just the snapshots).
      .catch(error => {
        console.error('[GPV3] Couldn\'t get query of organizations.\n' + error);
      });

    // do for every organization.
    for (var org of orgQuerySnap.docs) {
      let prjCreated = 0;
      let riskCreated = 0;
      let prjModified = 0;
      let riskModified = 0;

      // get all the orgIndexes because we want
      // the WIDs, to get the projects and risks.
      var orgIndexesQuerySnap = await admin.firestore().collection('orgIndex')
        .where('oid', '==', org.id).get()
        .catch(error => {
          console.error('[SP6H] Couldn\'t get query of orgIndexes.\n' + error);
        });

      for (var orgIndex of orgIndexesQuerySnap.docs) {
        var wid = orgIndex.data().wid;

        // do for creation time
        let prjQuerySnap;
        await admin.firestore().collection(model[3])
          .where('pid', '==', wid)
          .where('time', '>=', targetDate)
          .get().then(async (querySnap) => {
            prjCreated += querySnap.size;
            prjQuerySnap = querySnap;
          })
          .catch(error => {
            console.error('[SP7H] Couldn\'t get query of projects.\n' + error);
          });

        if (!prjQuerySnap.empty) {
          for (var project of prjQuerySnap.docs) {
            var pid = project.id
            await admin.firestore().collection(model[4])
              .where('pid', '==', pid)
              .where('time', '>=', targetDate)
              .get().then(async (querySnap) => { riskCreated += querySnap.size })
              .catch(error => {
                console.error('[SP8H] Couldn\'t get query of risks.\n' + error);
              });
          }
        } else console.warn('[SO8H] Projects query is empty for org: ' + org.id +
          ' and workspace: ' + orgIndex.data().wid + '.');

        // do for modification time
        let prjModQuerySnap;
        await admin.firestore().collection(model[3])
          .where('pid', '==', wid)
          .where('modified', '>=', targetDate)
          .get().then(async (querySnap) => {
            prjModified += querySnap.size;
            prjModQuerySnap = querySnap;
          })
          .catch(error => {
            console.error('[SP9H] Couldn\'t get query of projects.\n' + error);
          });

        if (!prjModQuerySnap.empty) {
          for (var project of prjModQuerySnap.docs) {
            var pid = project.id
            await admin.firestore().collection(model[4])
              .where('pid', '==', pid)
              .where('time', '>=', targetDate)
              .get().then(async (querySnap) => { riskCreated += querySnap.size })
              .catch(error => {
                console.error('[SP1I] Couldn\'t get query of risks.\n' + error);
              });
          }
        } else console.warn('[SO9H] Projects query is empty for org: ' + org.id +
          ' and workspace: ' + orgIndex.data().wid + '.');
      }

      // insert into firestore
      await admin.firestore().collection('orgSum').add({
        oid: org.id,
        created: {
          projects: prjCreated,
          risks: riskCreated
        },
        modified: {
          projects: prjModified,
          risks: riskModified
        },
        time: targetMilli
      }).catch(error => {
        console.error('[SP6H] Couldn\'t insert results in db.\n' + error);
      });
    }
    return null;
  });

module.exports = {
  removeSub,
  doubles,
  modelCleanup,
  summary
}
