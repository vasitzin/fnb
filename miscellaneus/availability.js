const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const validate = require('../authentication').validateToken;

const isNameFree = express();
isNameFree.use(cors({ origin: true }));
isNameFree.use(validate);

const insertMember = express();
insertMember.use(cors({ origin: true }));
insertMember.use(validate);

insertMember.post('/', async (req, res) => {
  let boolUser;
  let boolMem;

  await admin.auth().getUserByEmail(req.body.email)
    .then(() => { boolUser = true; })
    .catch(() => { boolUser = false; });

  res.status(200).send({ inMembers: boolMem, exists: boolUser });
});

isNameFree.post('/', async (req, res) => {
  let boolean;
  await admin.firestore().collection(req.body.col)
    .where('name', '==', req.body.name)
    .get().then(querySnap => {
      boolean = querySnap.empty;
    });
  res.status(200).send({ free: boolean });
});

module.exports = {
  isNameFree,
  insertMember
}