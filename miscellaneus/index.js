const functions = require('firebase-functions');
const region = 'europe-west3';

const availability = require('./availability');
const premiumJs = require('./premium');

const isNameFree = availability.isNameFree;
const insertMember = availability.insertMember;
const premium = premiumJs.premium;

module.exports = {
  isNameFree: functions.region(region).https.onRequest(isNameFree),
  insertMember: functions.region(region).https.onRequest(insertMember),
  premium: functions.region(region).https.onRequest(premium)
}