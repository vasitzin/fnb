const env = require('../env.json');
const admin = require('firebase-admin');
const stripe = require('stripe')(env.stripeKey);
const express = require('express');
const cors = require('cors');
const token = require('../authentication').validateToken;

const premium = express();
premium.use(
  cors({ origin: true }),
  token
);

premium.post('/', async (req, res) => {
  const uid = req.user.uid; // get user's id.

  // find how many times a uid is in the premium collection.
  let premiums = 0;
  await admin.firestore().collection('premiumIndex')
    .where('uid', '==', uid)
    .get().then(async (querySnap) => { premiums = querySnap.docs.length })
    .catch(error => {
      console.error(error);
      res.status(500).send().end();
    });

  if (premiums > 0) {
    console.error('[4G6I] Session is found in Premiums already.')
    res.status(400).send().end();
  }

  const session = await stripe.checkout.sessions.create({
    payment_method_types: ['card'],
    line_items: [{
      price: env.priceId,
      quantity: 1
    }],
    mode: 'subscription',
    success_url: env.urlSuccess,
    cancel_url: env.urlFailure
  }).catch(error => {
    console.error(error);
    res.status(500).send().end();
  });

  session.success_url = session.success_url + '?session=' + session.id;
  res.status(200).send(session);
});

premium.post('/success', async (req, res) => {
  const uid = req.user.uid; // get user's id.
  const session = await stripe.checkout.sessions.retrieve(req.body.id); // get stripe's checkout session.
  const sub = await stripe.subscriptions.retrieve(session.subscription); // get the subscrition.

  // find how many times a uid is in the premium collection.
  // this is to avoid duplicates.
  let premiums = 0;
  await admin.firestore().collection('premiumIndex')
    .where('uid', '==', uid)
    .get().then(async (querySnap) => { premiums = querySnap.docs.length })
    .catch(error => {
      console.error(error);
      res.status(500).send().end();
    });
  if (premiums > 1) console.error('[4G7I] A uid is found more than once in Premiums.');

  // find how many times a subscription is in the premium collection.
  // this is so that people can't share their 'keys'.
  let subs = 0;
  await admin.firestore().collection('premiumIndex')
    .where('subscription', '==', sub.id)
    .get().then(async (querySnap) => { subs = querySnap.docs.length })
    .catch(error => {
      console.error(error);
      res.status(500).send().end();
    });
  if (subs > 1) console.error('[5G7I] A subscription is found more than once in Premiums.');

  // if sub is active make user premium.
  if (sub.status === 'active' && premiums === 0 && subs === 0) {
    const { serverTimestamp } = admin.firestore.FieldValue;
    await admin.firestore().collection('premiumIndex').add({
      uid: uid,
      customer: session.customer,
      subscription: session.subscription,
      time: serverTimestamp()
    }).catch(error => {
      console.error(error);
      res.status(500).send().end();
    });
  }

  res.status(200).send();
});

premium.get('/isPremium', async (req, res) => {
  const uid = req.user.uid; // get user's id.

  await admin.firestore().collection('premiumIndex')
    .where('uid', '==', uid)
    .get().then(async (querySnap) => {
      // if non found return false,
      if (querySnap.docs.length == 0) res.status(200).send({ userPremium: false }).end();
      // if found return true.
      else res.status(200).send({ userPremium: true }).end();
    })
    .catch(error => {
      console.error(error);
      res.status(500).send().end();
    });
});

premium.get('/remove', async (req, res) => {
  const uid = req.user.uid; // get user's id.

  let deleted;
  await admin.firestore().collection('premiumIndex')
    .where('uid', '==', uid).get().then(async querySnap => {
      if (querySnap.docs.length == 0) {
        console.error('[6GG3] User not found in premium list!');
        res.status(500).send().end();
      }
      const customer = querySnap.docs[0].data().customer;
      const res = await stripe.customers.del(customer);
      deleted = res.deleted;
    }).catch(error => {
      console.error(error);
      res.status(500).send().end();
    });
  res.status(200).send({ deleted: deleted });
});

module.exports = {
  premium
}