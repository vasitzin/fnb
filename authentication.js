const admin = require('firebase-admin');

const model = require('./model/common').model;
const roles = require('./model/common').roles;

async function validateToken(req, res, next) {
  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))
    && !(req.cookies && req.cookies.__session)) {
    res.status(403).send();
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else if (req.cookies) {
    idToken = req.cookies.__session;
  } else {
    res.status(403).send();
    return;
  }

  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    req.user = decodedIdToken;
    next();
    return;
  } catch (error) {
    console.log(error);
    res.status(403).send();
    return;
  }
}

async function validateRole(req, res, next) {
  const method = req.method;
  const uid = req.user.uid;
  const here = model.indexOf(req.params.col);

  let id, pid;
  id = req.params.id;
  pid = req.params.pid;

  if (pid === undefined) {
    await admin.firestore().collection(model[here]).doc(id)
      .get().then(docRef => { pid = docRef.data().pid })
      .catch(error => {
        console.error(error)
        res.status(403).send('[0U5M] Unable to find item with given id.').end();
        return;
      });
  }

  switch (here) {
    case 0:
      if (uid === pid) { next(); return; }
      else {
        res.status(403).send('[0U6M] You can not interact with this Orgonization!').end();
        return;
      };
    case 1:
      res.status(403).send('[0U7M] You can not interact with users collection!').end();
      return;
    default:
      break;
  }

  const parents = await findParents(here, id, pid)
    .catch(error => {
      console.error(error);
      res.status(403).send('[0U8M] Owner could not be resolved.').end();
      return;
    });

  if (uid === parents.uid) { next(); return; }

  const role = await findRole(uid, parents.wid)
    .catch(error => {
      console.error('[1U7M] Organization Index query failed!' + error);
      res.status(403).send(error);
      return;
    });

  switch (method) {

    // In a GET request I want to permit everyone.
    case 'GET':
      next();
      return;

    // In a DELETE request I want editors to have permission,
    // but ONLY the owner can delete the workspace.
    case 'DELETE':
      if (here === 2) {
        res.status(403).send();
        return;
      }

    // In POST and PUT requests I want to NOT permit viewers.
    case 'POST':
    case 'PUT':
      if (role === roles[roles.length - 1]) {
        res.status(403).send();
        return;
      } else {
        next();
        return;
      }

    // In any other request method send error.
    default:
      res.status(403).send();
      return;
  }
}

async function findParents(here, id, pid) {
  let parents;

  if (here > 2) {
    await admin.firestore().collection(model[here - 1]).doc(pid)
      .get().then(docRef => {
        id = docRef.id;
        pid = docRef.data().pid;
      });
    parents = findParents(here - 1, id, pid);
  } else if (here === 2) parents = { uid: pid, wid: id };

  return parents;
}

async function findRole(uid, wid) {
  let role;
  await admin.firestore().collection('orgIndex').where('wid', '==', wid)
    .get().then(querySnap => {
      if (querySnap.empty)
        throw new Error('Access denied. This workspace ' + wid + ' isnt in an Organization.');
      else {
        const doc = querySnap.docs[0];
        editors = doc.data().editors;
        viewers = doc.data().viewers;
        if (editors.includes(uid)) {
          role = 'editors';
        } else if (viewers.includes(uid)) {
          role = 'viewers';
        } else {
          throw new Error('Access denied. This user ' + uid + ' isnt in the Organization\'s workspace.');
        }
      }
    });
  return role;
}

module.exports = {
  roles,
  validateToken,
  validateRole
}
