const functions = require('firebase-functions');
const region = 'europe-west3';

const poorModel = require('./poorModel');
const richModel = require('./richModel');
const orgIndex = require('./orgIndex');

const poor = poorModel.poorModel;
const rich = richModel.richModel;
const orgIn = orgIndex.orgIndex;

module.exports = {
  poorModel: functions.region(region).https.onRequest(poor),
  richModel: functions.region(region).https.onRequest(rich),
  orgIndex: functions.region(region).https.onRequest(orgIn)
}