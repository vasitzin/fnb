const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const token = require('../authentication').validateToken;
const role = require('../authentication').validateRole;
const common = require('./common');

const poorModel = express();
poorModel.use(
  cors({ origin: true }),
  token
);

let model = common.model;

poorModel.get('/:col/:pid/:order', role, async (req, res) => {
  const selectDoc = common.selectDoc;
  const docs = await selectDoc(req.params.col, req.params.pid, req.params.order, false)
    .catch(error => {
      console.log(error);
      res.status(500).send().end();
    });
  res.status(200).send(JSON.stringify(docs));
});

poorModel.post('/:col/:pid', role, async (req, res) => {
  const insertDoc = common.insertDoc;
  await insertDoc(req.params.col, req.body)
    .then(
      () => { res.status(201).send().end(); },
      error => {
        console.log(error);
        res.status(500).send().end();
      });
});

poorModel.delete('/:col/:id', role, async (req, res) => {
  const deleteDoc = common.deleteDoc;
  const col = req.params.col;
  const id = req.params.id;
  let timeStamp;
  if (model.indexOf(col) == -1) {
    res.status(404).send().end();
    return;
  }
  timeStamp = await deleteDoc(col, id, false).catch(error => {
    console.log(error);
    res.status(500).send().end();
  });

  if (col == model[0]) {
    const deleteOrgIndexes = common.deleteOrgIndexes;
    await deleteOrgIndexes(id).catch(error => {
      console.log(error);
      res.status(500).send().end();
    });
  }
  res.status(200).send(timeStamp);
});

poorModel.put('/:col/:id', role, async (req, res) => {
  const updateDoc = common.updateDoc;
  await updateDoc(req.params.col, req.params.id, req.body, false)
    .then(
      () => { res.status(200).send().end(); },
      error => {
        console.log(error);
        res.status(500).send().end();
      });
});

/**
 * @param {string} month target month this needs to be 0-11
 * @param {string} year target year
 * @returns an array of documents
 */
poorModel.post('/orgSum/:month/:year', async (req, res) => {
  const dateMilli = Date.UTC(req.params.year, req.params.month, 1);
  let docs = [];

  await admin.firestore().collection('orgSum')
    .where('oid', '==', req.body.oid)
    .where('time', '>=', dateMilli)
    .get().then(async (querySnap) => {
      for (doc of querySnap.docs) {
        let data = doc.data();
        docs.push({
          ...data
        });
      }
    }).catch(error => {
      console.log(error);
      res.status(500).send().end();
    });

  res.status(200).send(JSON.stringify(docs)).end();
});

module.exports = {
  poorModel
}