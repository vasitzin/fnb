const express = require('express');
const cors = require('cors');
const token = require('../authentication').validateToken;
const role = require('../authentication').validateRole;
const common = require('./common');

const richModel = express();
richModel.use(
  cors({ origin: true }),
  token
);

let model = common.model;

richModel.get('/:col/:pid/:order', role, async (req, res) => {
  const selectDoc = common.selectDoc;
  const docs = await selectDoc(req.params.col, req.params.pid, req.params.order, true)
    .catch(error => {
      console.error(error);
      res.status(500).send().end();
    });
  res.status(200).send(JSON.stringify(docs));
});

richModel.post('/:col/:pid', role, async (req, res) => {
  const insertDoc = common.insertDoc;
  const doc = await insertDoc(req.params.col, req.body)
    .catch(error => {
      console.error(error);
      res.status(500).send('Insert operation failed.').end();
    });
  res.status(201).send(JSON.stringify(doc));
});

richModel.post('/:col/:pid/:order', role, async (req, res) => {
  const insertDoc = common.insertDoc;
  const selectDoc = common.selectDoc;
  await insertDoc(req.params.col, req.body)
    .catch(error => {
      console.error(error);
      res.status(500).send('Insert operation failed.').end();
    });
  const docs = await selectDoc(req.params.col, req.body.pid, req.params.order, false)
    .catch(error => {
      console.error(error);
      res.status(500).send('Insert operation completed but failed to return data.').end();
    });
  res.status(201).send(JSON.stringify(docs));
});

richModel.delete('/:col/:id', role, async (req, res) => {
  const deleteDoc = common.deleteDoc;
  const col = req.params.col;
  const id = req.params.id;
  let timeStamp;
  switch (model.indexOf(col)) {
    case -1:
      res.status(404).send().end;
      break;
    case (model.length - 1):
      timeStamp = await deleteDoc(col, id, false).catch(error => {
        console.error(error);
        res.status(500).send().end;
      });
      break;
    default:
      timeStamp = await deleteDoc(col, id, true).catch(error => {
        console.error(error);
        res.status(500).send().end;
      });
  }
  res.status(200).send(timeStamp);
});

richModel.put('/:col/:id', role, async (req, res) => {
  const updateDoc = common.updateDoc;
  await updateDoc(req.params.col, req.params.id, req.body, true)
    .then(
      () => { res.status(200).send().end(); },
      error => {
        console.error(error);
        res.status(500).send().end();
      });
});

richModel.put('/organizations/remove/:id', async (req, res) => {
  const removeMember = common.removeMember;
  var emails = eval(req.body); // JSON in body -> [{'email': 'a@a.a'}, ... , {'email': 'z@z.z'}]
  for (var i = 0; i < emails.length; i++) {
    var email = emails[i].email;
    console.log(email); //TODO remove line
    await removeMember(req.params.id, email)
      .catch(error => {
        console.error(error);
        res.status(500).send().end();
      })
  }
  res.status(200).send({ succesful: true }).end();
});

module.exports = {
  richModel
}