const express = require('express');
const cors = require('cors');
const token = require('../authentication').validateToken;
//const role = require('../authentication').validateRole;
const common = require('./common');

const orgIndex = express();
orgIndex.use(
  cors({ origin: true }),
  token
);

orgIndex.get('/:oid/:wid', async (req, res) => {
  const selectOrgIndex = common.selectOrgIndex;
  const index = await selectOrgIndex(req.params.oid, req.params.wid)
    .catch(error => {
      console.log(error);
      res.status(500).send().end();
    });
  res.status(200).send(JSON.stringify(index));
});

orgIndex.get('/:oid', async (req, res) => {
  const selectOrgIndexs = common.selectOrgIndexes;
  const indexs = await selectOrgIndexs(req.params.oid)
    .catch(error => {
      console.log(error);
      res.status(500).send().end();
    });
  res.status(200).send(JSON.stringify(indexs));
});

orgIndex.post('/', async (req, res) => {
  const insertOrgIndex = common.insertOrgIndex;
  await insertOrgIndex(req.body)
    .then(
      () => { res.status(201).send().end(); },
      error => {
        console.log(error);
        res.status(500).send().end();
      });
});

orgIndex.delete('/:oid/:wid', async (req, res) => {
  const deleteOrgIndex = common.deleteOrgIndex;
  const oid = req.params.oid;
  const wid = req.params.wid;
  await deleteOrgIndex(oid, wid).catch(error => {
    console.log(error);
    res.status(500).send().end();
  });
  res.status(200).end();
});

orgIndex.put('/:oid/:wid', async (req, res) => {
  const updateOrgIndex = common.updateOrgIndex;
  await updateOrgIndex(req.params.oid, req.params.wid, req.body)
    .then(
      () => { res.status(200).send().end(); },
      error => {
        console.log(error);
        res.status(500).send().end();
      });
});

orgIndex.put('/:oid/:wid/:status', async (req, res) => {
  const updateOrgIndexRole = common.updateOrgIndexRole;
  const oid = req.params.oid;
  const wid = req.params.wid;
  const status = req.params.status;
  const email = req.body.email;
  const role = req.body.role;

  if (email == null)
    throw Error('[578S] No emails given. Aborting operation.');

  if (status != 'add' && status != 'remove')
    throw Error('[579S] Wrong status parameter detected, accepted values [add/remove].');

  await updateOrgIndexRole(oid, wid, status, email, role)
    .then(
      () => { res.status(201).send().end(); },
      error => {
        console.log(error);
        res.status(500).send().end();
      });
});

module.exports = {
  orgIndex
}