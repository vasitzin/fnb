const admin = require('firebase-admin');

const model = [
  "organizations",
  "users",
  "workspaces",
  "projects",
  "risks"
];

const roles = [
  "editors",
  "viewers"
];

/**
 * Recursive selection of all "sub"documents belonging
 * to the document whose id is given.
 * The first object in the returned array is the user,
 * only applicable for top level selections.
 * @param {string} col collection
 * @param {string} id 
 * @param {string} order field to use
 * @param {boolean} recurse 
 * @returns an array of documents
 */
async function selectDoc(col, id, order, recurse) {
  let docs = [];
  let here = model.indexOf(col);
  let displayName, userPhoto;

  let querySnap = await admin.firestore().collection(model[here])
    .where('pid', '==', id).orderBy(order.toString()).get()
    .catch(error => { throw error });

  //To do in workspace level only.
  if (here == 2) {
    // If we are in model[here] (workspaces) then "id" is the user id.
    // Try to find user info of the owner of this workspace...
    try {
      let user = await admin.auth().getUser(id);
      displayName = user.displayName;
      userPhoto = user.photoURL;
    } catch { console.error('User not found!'); };
    // and find workspaces that include them through Organizations.
    try {
      let docsFromIndexes = await wsFromIndexes(id, order, recurse);
      docs = docs.concat(docsFromIndexes);
    } catch { console.error('Error in index search!'); };
  }

  for (doc of querySnap.docs) {
    let id = doc.id;

    let orgName;
    if (here == 2) orgName = '';

    let data = doc.data();
    let children = [];
    if (recurse && here < (model.length - 1)) {
      children = await selectDoc(model[here + 1], id, order, recurse);
    }
    docs.push({ id, ...data, orgName, displayName, userPhoto, children });
  }
  return docs;
}

/**
 * Insert document with server timestamp to collection.
 * @param {string} col collection
 * @param {JSON} doc json obj contained in the body of the request
 */
async function insertDoc(col, doc) {
  const { serverTimestamp } = admin.firestore.FieldValue; // Create sentinel,

  // Append time field to end of doc obj.
  var docStr = JSON.stringify(doc); // Convert to string,
  docStr = docStr.slice(0, docStr.length - 1) + ',"time":""}'; // append,
  doc = JSON.parse(docStr); // back to json.

  const newDocRef = await admin.firestore().collection(col).add({
    ...doc,
    time: serverTimestamp() // give 'time' the sentinel.
  }).catch(error => { throw error; });

  const newDocSnap = await newDocRef.get();	// get the snapshot from the reference
  const id = newDocSnap.id;					// get the document's id from the snap
  const data = newDocSnap.data();				// get the rest of the data from the snap
  return { id, ...data };						// create and return the json with id and data
}

/**
 * Recursive delete of a document in given collection and of all
 * documents "linked" to it.
 * @param {string} col collection
 * @param {string} id
 * @param {boolean} recurse
 */
async function deleteDoc(col, id, recurse) {
  // Recurse and delete children documents first.
  if (recurse) {
    let next = model.indexOf(col) + 1;
    await admin.firestore().collection(model[next])
      .where('pid', '==', id)
      .get().then(async (querySnap) => {
        for (doc of querySnap.docs) {
          const id = doc.id;
          if (next < (model.length - 1)) {
            await deleteDoc(model[next], id, true)
              .catch(error => { throw error; });
          } else if (next == (model.length - 1)) {
            await deleteDoc(model[next], id, false)
              .catch(error => { throw error; });
          }
        }
      }).catch(error => { throw error; });
  }

  // If deleting an organization delete all relevant orgIndexes.
  if (model.indexOf(col) == 0) {
    await admin.firestore().collection('orgIndex').where('oid', '==', id).get()
      .then(async (querySnap) => {
        for (doc of querySnap.docs) {
          const id = doc.id;
          await admin.firestore().collection('orgIndex').doc(id).delete();
        }
      })
      .catch(error => { throw error; });
  }

  // If deleting a workspace delete all relevant orgIndexes.
  if (model.indexOf(col) == 2) {
    await admin.firestore().collection('orgIndex').where('wid', '==', id).get()
      .then(async (querySnap) => {
        for (doc of querySnap.docs) {
          const id = doc.id;
          await admin.firestore().collection('orgIndex').doc(id).delete();
        }
      })
      .catch(error => { throw error; });
  }

  // Finally delete document in question.
  return await admin.firestore().collection(col).doc(id).delete()
    .catch(error => { throw error; });
}

/**
 * Update a document in given collection.
 * @param {string} col 
 * @param {string} id 
 * @param {JSON} data the fields to be updated
 * @param {boolean} newTime update timestamp
 */
async function updateDoc(col, id, data, newTime) {
  const { serverTimestamp } = admin.firestore.FieldValue;
  await admin.firestore().collection(col).doc(id).update(data).then(
    async () => {
      if (newTime) await admin.firestore().collection(col).doc(id).update({
        modified: serverTimestamp()
      }).catch(error => { throw error; });
    }
  ).catch(error => { throw error; });
}

/**
 * Return a document from the orgIndex collection.
 * @param {String} oid 
 * @param {String} wid 
 * @returns index
 */
async function selectOrgIndex(oid, wid) {
  let index;
  let querySnap = await admin.firestore().collection('orgIndex')
    .where('oid', '==', oid).where('wid', '==', wid).get()
    .catch(error => { throw error });
  index = querySnap.docs[0];
  let id = index.id;
  let data = index.data();
  return { id, ...data };
}

/**
 * Return an array of indexes from the orgIndex array.
 * @param {String} oid 
 * @returns indexes array
 */
async function selectOrgIndexes(oid) {
  let indexes = [];
  let querySnap = await admin.firestore().collection('orgIndex')
    .where('oid', '==', oid).get()
    .catch(error => { throw error });
  for (index of querySnap.docs) {
    let id = index.id;
    let data = index.data();
    indexes.push({ id, ...data });
  }
  return indexes;
}

/**
 * Delete a single document from the orgIndex collection in db.
 * @param {String} oid 
 * @param {String} wid 
 */
async function deleteOrgIndex(oid, wid) {
  await admin.firestore().collection('orgIndex')
    .where('oid', '==', oid).where('wid', '==', wid).get()
    .then(async (querySnap) => {
      for (doc of querySnap.docs) {
        const id = doc.id;
        await admin.firestore().collection('orgIndex').doc(id).delete();
      }
    })
    .catch(error => { throw error; });
}

/**
 * Delete multiple documents from the orgIndex collection in db.
 * @param {String} oid 
 */
async function deleteOrgIndexes(oid) {
  await admin.firestore().collection('orgIndex')
    .where('pid', '==', oid).get()
    .then(async (querySnap) => {
      for (doc of querySnap.docs) {
        const id = doc.id;
        await admin.firestore().collection('orgIndex').doc(id).delete();
      }
    })
    .catch(error => { throw error; });
}

/**
 * A json object to insert as a new document in db.
 * @param {JSON} doc 
 */
async function insertOrgIndex(doc) {
  await admin.firestore().collection('orgIndex').add({
    ...doc
  }).catch(error => { throw error; });
}

/**
 * Update a document in orgIndex collection.
 * @param {String} oid 
 * @param {String} wid 
 * @param {JSON} data 
 */
async function updateOrgIndex(oid, wid, data) {
  let querySnap = await admin.firestore().collection('orgIndex')
    .where('oid', '==', oid)
    .where('wid', '==', wid)
    .get().catch(error => { throw error; });
  let docId = querySnap.docs[0].id;
  await admin.firestore().collection('orgIndex')
    .doc(docId)
    .update(data)
    .catch(error => { throw error; });
}

/**
 * Remove the user with this email from org and all matching orgIndexes.
 * @param {*} oid the organization's id,
 * @param {*} email a user's email.
 */
async function removeMember(oid, email) {
  // Get the uid of the user with  this email.
  let uid = await getUidFromEmail(email);

  let querySnap = await admin.firestore().collection('orgIndex')
    .where('oid', '==', oid).get();
  if (querySnap.size == 0) throw Error('[F86S] No matching index found.');

  for (index of querySnap.docs) {
    let indexId = index.id;
    let indexViewers = index.data().viewers;
    let indexEditors = index.data().editors;

    try {
      if (indexViewers.includes(uid)) await removeFromArrayViewers(indexId, uid).then(() => { console.log('removed viewer ' + uid); });
      else if (indexEditors.includes(uid)) await removeFromArrayEditors(indexId, uid).then(() => { console.log('removed editor ' + uid); });
      else console.error('//TODO weird though');
    } catch (error) { console.error(error); }
  }

  await admin.firestore()
    .collection(model[0]).doc(oid)
    .update({ members: admin.firestore.FieldValue.arrayRemove(email) })
    .catch(error => { console.error(error) });
}

/**
 * Update the user's, with this email, role in the org.
 * @param {*} oid the organization's id,
 * @param {*} email a user's email.
 */
async function updateOrgIndexRole(oid, wid, status, email, role) {
  // Get the uid of the user with  this email.
  let uid = await getUidFromEmail(email);

  // Get the QuerySnapshot of the matching orgIndex collection,
  // from the organization's and workspace's ids.
  let querySnap = await getQuerySnap(oid, wid);

  let indexId = querySnap.docs[0].id;
  let indexViewers = querySnap.docs[0].data().viewers;
  let indexEditors = querySnap.docs[0].data().editors;

  switch (role) {
    case roles[0]:
      switch (status) {
        case 'add':
          if (!indexEditors.includes(uid)) {
            if (indexViewers.includes(uid)) await removeFromArrayViewers(indexId, uid);
            await addToArrayEditors(indexId, uid);
          }
        case 'remove':
          if (indexEditors.includes(uid)) await removeFromArrayEditors(indexId, uid);
      }
      break;
    case roles[1]:
      switch (status) {
        case 'add':
          if (!indexViewers.includes(uid)) {
            if (indexEditors.includes(uid)) await removeFromArrayEditors(indexId, uid);
            await addToArrayViewers(indexId, uid);
          }
        case 'remove':
          if (indexViewers.includes(uid)) await removeFromArrayViewers(indexId, uid);
      }
      break;
    default:
      throw Error('[8D23] Wrong role parameter detected.');
  }
}

async function wsFromIndexes(uid, order, recurse) {
  let here = 2;
  let docs = [];

  for (role of roles) {

    // Get the snapshots of the query in the orgIndex collection with matching user ids.
    let querySnapIndexes = await admin.firestore().collection('orgIndex')
      .where(role, 'array-contains', uid).get()
      .catch(error => { throw error });

    // For each snapshot of the query from before, make an obj and push it to docs[].
    for (index of querySnapIndexes.docs) {
      let oid = ''; // Organization id, from orgIndex.
      oid = index.data().oid;

      // Organization name, from organizations.
      let orgName = '';
      try {
        var tempObj = await admin.firestore().collection(model[0]).doc(oid).get();
        orgName = tempObj.data().name;
      } catch { console.log('[06Y4] Organization name not found from indexing system.'); };

      // Get the docSnap of a workspace obj.
      let wid = index.data().wid;
      let doc = await admin.firestore().collection(model[2]).doc(wid).get();

      let id = doc.id;		// Get the workspace's id. (though i kinda already have it)
      let data = doc.data();	// Get the rest of the data.

      // Try to get the displayName and photoUrl of the user that this workspace belongs to.
      let displayName = '';
      let photoUrl = '';
      try {
        let user = await admin.auth().getUser(data.pid);
        displayName = user.displayName;
        photoUrl = user.photoURL;
      } catch { console.log('[2U88] User not found frow indexing system.'); };

      // Get the children of this workspace.
      let children = [];
      if (recurse) {
        children = await selectDoc(model[here + 1], id, order, recurse);
      }

      docs.push({ id, ...data, oid, orgName, role, displayName, photoUrl, children }); // Push it in.
    }
  }
  return docs;
}

async function getUidFromEmail(email) {
  let uid;
  await admin.auth().getUserByEmail(email)
    .then((userRecord) => { uid = userRecord.uid })
    .catch((error) => { console.log('Error fetching user data:', error); });
  return uid;
}

async function getQuerySnap(oid, wid) {
  let querySnap = await admin.firestore().collection('orgIndex')
    .where('oid', '==', oid)
    .where('wid', '==', wid)
    .get();
  if (querySnap.size == 0) throw Error('[F87S] No matching index found.');
  if (querySnap.size > 1) console.error('[F88S] More than 1 matching indexs found.');
  return querySnap;
}

async function addToArrayViewers(indexId, uid) {
  await admin.firestore()
    .collection('orgIndex')
    .doc(indexId)
    .update({ viewers: admin.firestore.FieldValue.arrayUnion(uid) });
}

async function removeFromArrayViewers(indexId, uid) {
  await admin.firestore()
    .collection('orgIndex')
    .doc(indexId)
    .update({ viewers: admin.firestore.FieldValue.arrayRemove(uid) });
}

async function addToArrayEditors(indexId, uid) {
  await admin.firestore()
    .collection('orgIndex')
    .doc(indexId)
    .update({ editors: admin.firestore.FieldValue.arrayUnion(uid) });
}

async function removeFromArrayEditors(indexId, uid) {
  await admin.firestore()
    .collection('orgIndex')
    .doc(indexId)
    .update({ editors: admin.firestore.FieldValue.arrayRemove(uid) });
}

module.exports = {
  model,
  roles,
  selectDoc,
  deleteDoc,
  insertDoc,
  updateDoc,
  selectOrgIndex,
  selectOrgIndexes,
  deleteOrgIndex,
  deleteOrgIndexes,
  insertOrgIndex,
  updateOrgIndex,
  removeMember,
  updateOrgIndexRole
}